#include <cmath>
#include <iomanip>
#include <iostream>


// #include<conio.h>
// #include<math.h>

using namespace std;

double distanceCalculate(double x1, double y1, double x2, double y2)
{
    double x = x1 - x2;
    double y = y1 - y2;
    double dist;

    dist = pow(x,2)+pow(y,2);           //calculating distance by euclidean formula
    dist = sqrt(dist);                  //sqrt is function in math.h

    return dist;
}

int main()
{
    double x1, y1, x2, y2;
    double dist;
    cout<<"First point's x-coordinate: ";           //user input needed
    cin>>x1;
    cout<<"First point's y-coordinate: ";
    cin>>y1;
    cout<<"Second point's x-coordinate: ";
    cin>>x2;
    cout<<"Second point's y-coordinate: ";
    cin>>y2;

    dist = distanceCalculate(x1, y1 , x2, y2);          //calling distanceCalculate formula
    cout<<"The euclidean distance between the two points is "<<std::round(dist*10000)/10000<<"."<<std::endl;
}