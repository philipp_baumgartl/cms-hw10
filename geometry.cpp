#define _USE_MATH_DEFINES
#include <cmath>
#include <iomanip>
#include <iostream>


double surface_cylinder(double r, double h) {
    double result;

    result = 2* r * r * M_PI + 2 *r * M_PI * h;
    return result;
}

double volume_cylinder(double r, double h) {
    double result;

    result = r *r * M_PI * h;
    return result;
}

double surface_cone(double r, double h) {
    double result;
    double s;
    
    s = std::pow(h * h + r * r,0.5);
    result = r * s * M_PI + r * r * M_PI;
    return result;
}

double volume_cone(double r, double h) {
    double result;

    result =  r *r * M_PI * h / 3;
    return result;
}

int main(int argc, char** argv) {
  if (argc != 3) {
    std::cout << "Usage: " << argv[0] << " radius height\n";
    return 1;
  }
  double radius(std::atof(argv[1]));
  double height(std::atof(argv[2]));
  std::cout << std::fixed << std::setprecision(2);
  std::cout << "surface_cylinder(" << radius << ", " << height << "): ";
  std::cout << surface_cylinder(radius, height) << "\n";
  std::cout << "volume_cylinder(" << radius << ", " << height << "): ";
  std::cout << volume_cylinder(radius, height) << "\n";
  std::cout << "surface_cone(" << radius << ", " << height << "): ";
  std::cout << surface_cone(radius, height) << "\n";
  std::cout << "volume_cone(" << radius << ", " << height << "): ";
  std::cout << volume_cone(radius, height) << "\n";
  return 0;
}
