// TODO: add required header includes

#include <iostream>
#include <iomanip>


const double inch_per_meter = 100 / 2.54;
const double feet_per_meter = inch_per_meter / 12;
const double yard_per_meter = feet_per_meter / 3;
const double miles_per_meter = yard_per_meter / 1760;



int main() {
  double meter;
  std::cout<<"Enter a distance in meters: ";
  std::cin>>meter;
  std::cout << std::setprecision(2) << std::fixed;
  std::cout<<meter*inch_per_meter<<" inch"<<std::endl;
  std::cout<<meter*feet_per_meter<<" feet"<<std::endl;
  std::cout<<meter*yard_per_meter<<" yards"<<std::endl;
  std::cout<<meter*miles_per_meter<<" miles"<<std::endl;
  
  // TODO: implement your solution here
  return 0;
}
