/**
 * Most basic program in C++
 */

#include <iostream>  // allows using std::cout and std::endl

int main() {  // main function - C++ programs start their execution here
    
  double result (1 + 5 * 6);
  std::cout << result << std::endl;  // writing to stdout
  return 0;  // indicate successful termination to environment
}  // blocks are delimited with curly braces