#include <cmath>
#include <iostream>
#include <utility>


std::pair<double, double> quadratic(double a, double b, double c) {
    double root;
    double x1;
    double x2;
    std::pair <double, double> result;
    
    root = b * b - 4 * a * c;
    x1  = (-b + std::pow(root,0.5)) / (2 * a);
    x2  = (-b - std::pow(root,0.5)) / (2 * a);
    
    result = std::make_pair(x1,x2);
    return result;
}


int main(int argc, char** argv) {
  if (argc != 4) {
    std::cout << "Usage: " << argv[0] << " a b c\n";
    return 1;
  }
  double a(std::atof(argv[1]));
  double b(std::atof(argv[2]));
  double c(std::atof(argv[3]));

  std::pair<double, double> result(quadratic(a, b, c));
  std::cout << "x1: " << result.first << "; x2: " << result.second << "\n";
  return 0;
}


// import math
//
//
// def quadratic(a, b, c):
// """Compute the solution to a quadratic equation."""
// root = b ** 2 - 4 * a * c
//
// if root >= 0:
//   sqrt = math.sqrt
//   else:
//     sqrt = cmath.sqrt
//     x1 = (-b + sqrt(root)) / (2 * a)
//     x2 = (-b - sqrt(root)) / (2 * a)
//     return x1, x2
