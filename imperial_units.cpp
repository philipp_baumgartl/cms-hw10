// TODO: add required header includes

#include <cmath>
#include <iomanip>
#include <iostream>

using namespace std;

const double inch_per_meter = 100 / 2.54;

int main() {
  // TODO: implement your solution here
  double meter;

  cout << std::setprecision(2) << std::fixed;
  cout << "Enter a distance in meters: ";
  cin >> meter;
  
  
  double inch;
  double feet;
  double yards;
  double miles;
  
  inch = meter * inch_per_meter;
  feet = inch/12;
  yards = feet/3;
  miles = yards/1760;
  
  cout << inch << " inch" << "\n" << feet << " feet" << "\n" << yards << " yards" << "\n" << miles << " miles" << "\n";
  
  return 0;
}
